const http = require('http');
const port = 3000;
const ip = 'localhost';

const buscar = require('./src/components/buscar');

var url = require('url');

const server = http.createServer((req, res) => {

    res.writeHead(200, { 'Content-Type': 'application/json' });

    var query = url.parse(req.url, true).query;

    buscar.initSearch(query.lat, query.lng).then(data => {
        res.end(JSON.stringify(data));
    });   

});

server.listen(port, ip, () => {
    console.log(`Servidor rodando em http://${ip}:${port}`);
})
const https = require('https');

module.exports.initSearch = function (lat, lng) {

    let latitude_input = lat;
    let longitude_input = lng;

    let url_addresses = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude_input},${longitude_input}&sensor=false&key=AIzaSyA6TfU84r6wT2gu1NYAOCN7JkO342K21So
`;

    let url_places = `https://maps.googleapis.com/maps/api/place/search/json?location=${latitude_input},${longitude_input}&radius=10000&type=gas_station&key=AIzaSyA6TfU84r6wT2gu1NYAOCN7JkO342K21So
`;


    let getData = (url) => new Promise((resolve, reject) => {

        var req = https.get(url, (resp) => {

            if (resp.statusCode < 200 || resp.statusCode >= 300) {
                return reject(new Error('statusCode=' + resp.statusCode));
            }

            let data = '';

            resp.on('data', (chuck) => {
                data += chuck;
            });

            resp.on('end', () => {

                resolve(JSON.parse(data));

            });

        }).on("error", (err) => {
            console.log("Error: " + err.message);
            reject(err);
        }).end();

    });

    return Promise.all([getData(url_addresses), getData(url_places)]).then(result => {

        var address = serializeAdresses(result[0]);

        var places = serializePlaces(result[1]);

        address = Object.assign( address, {postos : places} );

        return address;
    }).catch(reason => {
        console.warn('Failed!', reason);
    });

    function serializeAdresses(adresses) {

        var address = adresses.results.filter(address => address.geometry.location.lat == latitude_input && address.geometry.location.lng == longitude_input);

        const serializedAddress = address.map(address => {
            return {
                lat: address.geometry.location.lat,
                lng: address.geometry.location.lng,
                endereco:
                    {
                        estado: address.address_components[4].short_name,
                        cidade: address.address_components[3].long_name,
                        bairro: address.address_components[2].long_name,
                        logradouro: address.address_components[1].long_name,
                        numero: address.address_components[0].long_name,
                        cep: address.address_components[6].long_name
                    }
            }
        });

        return serializedAddress[0];
    }

    function serializePlaces(places) {

        const serializedPlaces = places.results.map(place => {
            return {
                lat: place.geometry.location.lat,
                lng: place.geometry.location.lng,
                nome: place.name,
                endereco: place.vicinity
            }
        });

        return serializedPlaces;

    }

}
